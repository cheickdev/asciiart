import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int letterWidth = in.nextInt();
        int letterHeight = in.nextInt();
        in.nextLine();

        String wordToConvert = in.nextLine();

        Alphabet alphabet = buildAlphabetFromInput(in, letterWidth, letterHeight);

        Word asciiWord = alphabet.convertWordToAsciiWord(wordToConvert);

        asciiWord.print(letterHeight);
    }

    static Alphabet buildAlphabetFromInput(Scanner in, int letterWidth, int letterHeight) {
        Alphabet alphabet = new Alphabet(letterWidth, letterHeight);
        for (int h = 0; h < letterHeight; h++) {
            String line = in.nextLine();
            System.err.println(line);
            int lineCursor = 0;
            for (int letter = 0; letter < Alphabet.NUMBER_OF_ALPHABET; letter++) {
                for (int w = 0; w < letterWidth; w++) {
                    alphabet.alphabets[letter].setLetterCharAt(w, h, line.charAt(lineCursor++));
                }
            }
        }
        return alphabet;
    }
    static class Alphabet {
        static final int NUMBER_OF_ALPHABET = 27;
        static final int UNKNOWN_CHAR_INDEX = 26;
        static final char FIRST_UPPERCASE_CHAR = 'A';
        static final char LAST_UPPERCASE_CHAR = 'Z';
        static final char FIRST_LOWERCASE_CHAR = 'a';
        static final char LAST_LOWERCASE_CHAR = 'z';

        int letterWidth;
        int letterHeight;
        Letter[] alphabets;

        Alphabet(int letterWidth, int letterHeight) {
            this.letterWidth = letterWidth;
            this.letterHeight = letterHeight;
            this.alphabets = new Letter[NUMBER_OF_ALPHABET];
            initializeLetters();
        }

        private void initializeLetters() {
            for (int i = 0; i < NUMBER_OF_ALPHABET; i++) {
                this.alphabets[i] = new Letter(letterWidth, letterHeight);
            }
        }

        Word convertWordToAsciiWord(String word) {
            Word asciiWord = new Word(word.length());
            for (int i = 0; i < word.length(); i++) {
                Letter letter = getLetter(word.charAt(i));
                asciiWord.append(letter);
            }
            return asciiWord;
        }

        private Letter getLetter(char c) {
            int index = getCharIndex(c);
            return alphabets[index];
        }

        private int getCharIndex(char c) {
            int index = UNKNOWN_CHAR_INDEX;
            if (isLowercase(c)) {
                index = getIndexFormLowercaseChar(c);
            } else if (isUppercase(c)) {
                index = getIndexFormUppercaseChar(c);
            }
            return index;
        }

        private int getIndexFormUppercaseChar(char c) {
            return c - FIRST_UPPERCASE_CHAR;
        }

        private int getIndexFormLowercaseChar(char c) {
            return c - FIRST_LOWERCASE_CHAR;
        }

        private boolean isUppercase(char c) {
            if (c >= FIRST_UPPERCASE_CHAR) {
                if (c <= LAST_UPPERCASE_CHAR) {
                    return true;
                }
            }
            return false;
        }

        private boolean isLowercase(char c) {
            if (c >= FIRST_LOWERCASE_CHAR) {
                if (c <= LAST_LOWERCASE_CHAR) {
                    return true;
                }
            }
            return false;
        }
    }

    static class Word {
        int width;
        int cursor;
        Letter[] letters;

        Word(int width) {
            this.width = width;
            this.cursor = 0;
            this.letters = new Letter[width];
        }

        boolean append(Letter letter) {
            if (cursor == width) {
                return false;
            }
            letters[cursor++] = letter;
            return true;
        }

        void print(int letterHeight) {
            for (int h = 0; h < letterHeight; h++) {
                for (int letterIndex = 0; letterIndex < cursor; letterIndex++) {
                    Letter letter = letters[letterIndex];
                    System.out.print(letter.getRowText(h));
                }
                System.out.println();
            }
        }
    }

    static class Letter {
        int width;
        int height;
        char[][] letterMatrix;

        public Letter(int width, int height) {
            this.width = width;
            this.height = height;
            this.letterMatrix = new char[width][height];
        }

        String getRowText(int row) {
            String rowText = "";
            for (int i = 0; i < width; i++) {
                rowText += letterMatrix[i][row];
            }
            return rowText;
        }

        void setLetterCharAt(int w, int h, char c) {
            letterMatrix[w][h] = c;
        }
    }
}